﻿using UnityEngine;

public class BattleMusic : MonoBehaviour {

    public AudioClip Countdown;
    public AudioClip MusicHeader;
    public AudioClip Loop;

    public AudioSource AudioSource1;
    public AudioSource AudioSource2;

    private bool _isLooping;

    public void Awake()
    {
        AudioSource1.clip = Countdown;
        AudioSource2.clip = MusicHeader;
        AudioSource1.Play();
        AudioSource2.Play();
    }

    public void FixedUpdate()
    {
        if (!AudioSource2.isPlaying && !_isLooping)
        {
            AudioSource1.clip = Loop;
            AudioSource1.loop = true;
            AudioSource1.Play();
            _isLooping = true;
        }
    }
}
