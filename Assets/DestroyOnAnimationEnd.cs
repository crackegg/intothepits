﻿using UnityEngine;

public class DestroyOnAnimationEnd : MonoBehaviour {

    private Animator _animator;

    public void Awake()
    {
        _animator = GetComponent<Animator>();
    }

	void Update () {
		if (_animator.GetCurrentAnimatorStateInfo(0).IsName("End"))
            Destroy(gameObject);
	}
}
