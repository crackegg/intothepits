﻿namespace Assets
{
    public interface IAbility
    {
        void Trigger(Player player);
    }
}
