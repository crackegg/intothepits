﻿using UnityEngine;

namespace Assets
{
    public class AnimationManager
    {
        private static AnimationManager _instance;

        public static AnimationManager Instance()
        {
            if (_instance == null)
                _instance = new AnimationManager();

            return _instance;
        }

        public void ShowFrontImpact(Vector2 position)
        {
            GameObject.Instantiate(Resources.Load("ImpactFrontAnimation", typeof(GameObject)), position, Quaternion.identity);
        }

        public void ShowSideImpact(Vector2 position)
        {
            GameObject.Instantiate(Resources.Load("ImpactSideAnimation", typeof(GameObject)), position, Quaternion.identity);
        }

        public void ShowLandImpact(Vector2 position)
        {
            GameObject.Instantiate(Resources.Load("LandAnimation", typeof(GameObject)), position, Quaternion.identity);
        }
    }
}
