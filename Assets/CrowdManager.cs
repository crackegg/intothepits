﻿using System;
using UnityEngine;

namespace Assets
{
    public delegate void CrowdEventHandler(object sender, EventArgs e);

    public class CrowdManager : MonoBehaviour
    {
        public AudioSource LowHealthAudioSource;
        public AudioSource InstantCheers;

        public AudioClip CrowdBackground;
        public AudioClip CrowdDeathExclamation;
        public AudioClip CrowdCheersLowHealth;
        public AudioClip[] CrowdSpecialLanded;

        public void PlayerDropedToLowHealth(object sender, EventArgs e)
        {
            if (!LowHealthAudioSource.isPlaying)
                LowHealthAudioSource.Play();
        }

        public void PlayerComeBackToHighHealth(object sender, EventArgs e)
        {
            LowHealthAudioSource.Stop();
        }

        public void PlayerLandedSpecialMove(object sender, EventArgs e)
        {
            if (!InstantCheers.isPlaying)
            {
                InstantCheers.clip = CrowdSpecialLanded[UnityEngine.Random.Range(0,2)];
                InstantCheers.Play();
            }
        }

        public void PlayerDied(object sender, EventArgs e)
        {
            if (!InstantCheers.isPlaying)
            {
                InstantCheers.clip = CrowdDeathExclamation;
                InstantCheers.Play();
            }
        }
    }
}
