﻿using UnityEngine;

namespace Assets
{
    public class ChargeAbility : MonoBehaviour, IAbility
    {
        public event CrowdEventHandler PlayerLandedSpecialMove;
        private Animator _animator;
        private Rigidbody2D _rigidbody;
        private Player _player;
        private GrabedPlayer _grabedPlayer;

        public bool IsCharging;

        public void Awake()
        {
            var crowdManager = GameObject.Find("Crowd").GetComponent<CrowdManager>();
            PlayerLandedSpecialMove += crowdManager.PlayerLandedSpecialMove;
            _animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody2D>();
            _player = GetComponent<Player>();
        }

        public void Trigger(Player player)
        {
            player.State = PlayerState.STUNNED;
            IsCharging = true;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Floor")) return;

            if (collision.gameObject.layer == LayerMask.NameToLayer("Player") && _grabedPlayer == null && IsCharging)
            {
                var grabedPlayer = collision.gameObject.GetComponent<Player>();
                var chargeAbility = collision.gameObject.GetComponent<ChargeAbility>();
                if (chargeAbility != null && chargeAbility.IsCharging)
                {
                    chargeAbility.IsCharging = false;
                    IsCharging = false;
                    grabedPlayer.KnockbackFrom(_player.transform.position);
                    grabedPlayer.StartAbilityCooldown();
                    _player.KnockbackFrom(grabedPlayer.transform.position);
                    _player.StartAbilityCooldown();
                    
                    return;
                }
                
                var rigidbody = collision.gameObject.GetComponent<Rigidbody2D>();
                var collider = collision.gameObject.GetComponent<CapsuleCollider2D>();
                var animator = collision.gameObject.GetComponent<Animator>();
                _grabedPlayer = new GrabedPlayer { Player = grabedPlayer, Rigidbody = rigidbody, Animator = animator, ChargeAbility = chargeAbility };
                _grabedPlayer.Player.State = PlayerState.STUNNED;
                _grabedPlayer.Player.UpdatePlayerSideAfterImpact(_player.transform.position.x);
                _grabedPlayer.Animator.SetBool("HitReceived", true);
                PlayerLandedSpecialMove(this, null);

            }

            if (collision.gameObject.layer == LayerMask.NameToLayer("Wall") && IsCharging)
            {
                _player.KnockbackFrom(collision.transform.position);
                AnimationManager.Instance().ShowSideImpact(collision.contacts[0].point);
                _player.StartAbilityCooldown(1f);
                IsCharging = false;
            }
        }

        public void Update()
        {
            _animator.SetBool("IsCharging", IsCharging);  
        }

        public void FixedUpdate()
        {
            if (IsCharging)
            {
                var currentVelocity = _rigidbody.velocity;
                currentVelocity.x = _player.GetDirectionModifier() * _player.Speed * 1.5f * Time.deltaTime;
                if (_grabedPlayer != null)
                {
                    if (_grabedPlayer.Player.CollidesWithWall) {
                        _grabedPlayer.Player.SuffersDamage(_player.AbilityDamage, _player.transform.position);
                        _grabedPlayer.Player.State = PlayerState.STUNNED;
                        _grabedPlayer.Player.RecoverIn(1f);
                        _player.State = PlayerState.IDLE;
                        IsCharging = false;
                        _player.StartAbilityCooldown();
                        _grabedPlayer = null;

                        return;
                    }
                    _grabedPlayer.Rigidbody.velocity = currentVelocity;
                }

                _rigidbody.velocity = currentVelocity;
            }
        }
    }

    public class GrabedPlayer
    {
        public Player Player { get; set; }
        public Rigidbody2D Rigidbody { get; set; }
        public Animator Animator { get; set; }
        public ChargeAbility ChargeAbility { get; set; }

        public void Follow(Vector2 position)
        {
            Rigidbody.MovePosition(position);
        }
    }
}
