﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class Countdown : MonoBehaviour
    {
        public float TimeLeft = 5 * 60;

        private Text _text;

        public void Awake()
        {
            _text = GetComponent<Text>();
        }

        public void Update()
        {
            TimeLeft -= Time.deltaTime;
            var roundedTimeLeft = Mathf.Round(TimeLeft);
            _text.text = Mathf.Round((roundedTimeLeft-60) / 60).ToString() + ":" + (roundedTimeLeft % 60).ToString("00");
        }
    }
}
