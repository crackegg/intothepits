﻿using Assets;
using System;
using UnityEngine;

public class Move : MonoBehaviour {

    public Player Player;

    private Transform _groundCheck;
    private Transform _leftCheck;
    private Transform _rightCheck;
    private Transform _attackCheck;
    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidbody2D;

    private Vector3 _attackCheckPosition = new Vector3(0.07f, 0, 0);

    public void Awake()
    {
        _leftCheck = transform.Find("LeftCheck");
        _rightCheck = transform.Find("RightCheck");
        _attackCheck = transform.Find("AttackCheck");
        _groundCheck = transform.Find("GroundCheck");
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        Player = GetComponent<Player>();

    }

    public void Update()
    {
        Player.IsGrounded = CheckIfGrounded();
    }

    public void FixedUpdate()
    {
        var force = _rigidbody2D.velocity;

        switch (Player.State)
        {
            case PlayerState.JUMPING:
                force.y += Player.JumpPower * Time.deltaTime;
                force.x = GetMovementVelocity(Player, force.x);
                Player.State = PlayerState.IDLE;
                _rigidbody2D.velocity = force;
                break;

            case PlayerState.STUNNED:
                break;

            default:
                force.x = GetMovementVelocity(Player, force.x);
                _rigidbody2D.velocity = force;
                break;
        }
    }

    private float GetMovementVelocity(Player player, float currentVelocity)
    {
        if (Player.HorizontalMovement != 0)
            return (Player.HorizontalMovement/Math.Abs(Player.HorizontalMovement)) * Player.Speed * Time.deltaTime;
        else
            return currentVelocity;
    }

    public bool CheckIfGrounded()
    {
        return Physics2D.Linecast(transform.position, _groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
    }
}
