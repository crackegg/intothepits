﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets
{
    public interface IAttackBehaviour
    {
        void Behave(Player player);
    }

    public class MeleeAttack : MonoBehaviour, IAttackBehaviour
    {
        private Transform _attackCheck;

        private List<GameObject> _hitBoxes;

        public void Awake()
        {
            _attackCheck = transform.Find("AttackCheck");
            _hitBoxes = new List<GameObject>();
        }

        public void Behave(Player player)
        {
            var hitbox = Instantiate(Resources.Load(player.HitBox, typeof(GameObject)), _attackCheck.position, Quaternion.identity) as GameObject;
            var hitBoxComponent = hitbox.gameObject.GetComponent<Hitbox>();

            hitBoxComponent.Source = transform.gameObject.GetComponent<Player>();
            hitBoxComponent.Damage = player.Damage;
            _hitBoxes.Add(hitbox);

            Invoke("RemoveHitBoxes", player.AttackDuration);
        }

        private void RemoveHitBoxes()
        {
            _hitBoxes.ForEach(x => Destroy(x));
            _hitBoxes.Clear();
        }
    }
}
