﻿using System;
using UnityEngine;

namespace Assets
{
    public class InputManager : MonoBehaviour
    {
        public string HorizontalInput = "Horizontal_P1";
        public string JumpInput = "Jump_P1";
        public string AttackInput = "Attack_P1";
        public string AbilityInput = "Charge_P1";

        public void HandleInputs(Player player, Animator animator)
        {
            if (Input.GetButtonDown(AbilityInput) && !player.AbilityIsOnCooldown && player.IsGrounded)
            {
                player.TriggerAbility();
            }

            if (Input.GetButtonDown(JumpInput) && player.IsGrounded)
            {
                player.State = PlayerState.JUMPING;
                animator.SetTrigger("Jump");
            }
            else if (Input.GetButtonDown(AttackInput) && !player.AttackIsOnCooldown)
            {
                animator.SetTrigger("Attack");
                player.StartAttackCooldown();
            }
        }

        public void HandleMovement(Player player)
        {
            player.HorizontalMovement = Input.GetAxis(HorizontalInput) * player.Speed;
        }
    }
}
