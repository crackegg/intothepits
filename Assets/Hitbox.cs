﻿using UnityEngine;

namespace Assets
{
    public class Hitbox : MonoBehaviour
    {
        public float Damage;
        public Player Source;

        public void OnTriggerEnter2D(Collider2D coll)
        {
            var collidingPlayer = coll.gameObject.GetComponent<Player>();
            if (collidingPlayer != Source && coll.gameObject.layer == LayerMask.NameToLayer("Player"))
            {
                AnimationManager.Instance().ShowFrontImpact(transform.position);
                collidingPlayer.SuffersDamage(Damage, Source.transform.position);
                Destroy(gameObject);
            }
        }
    }
}
