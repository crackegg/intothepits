﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class HealthBar : MonoBehaviour
    {
        public Player Player;

        private Text _text;
        
        public void Awake()
        {
            _text = GetComponent<Text>();
        }

        public void Update()
        {
            _text.text = Player.CurrentHealth.ToString();
        }
    }
}
