﻿using System;
using UnityEngine;

namespace Assets
{
    public static class Utils
    {
        public static bool CollidesWith(string layer, Transform startingPoint, Transform check)
        {
            return Physics2D.Linecast(startingPoint.position, check.position, 1 << LayerMask.NameToLayer(layer));
        }

        public static bool IsFacingRight(Transform playerTransform, Transform attackCheckTransform)
        {
            return playerTransform.position.x < attackCheckTransform.position.x;
        }
    }
}
