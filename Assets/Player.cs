﻿
using System;
using UnityEngine;

namespace Assets
{
    public class Player : MonoBehaviour
    {
        public event CrowdEventHandler PlayerDropedToLowHealth;
        public event CrowdEventHandler PlayerComeBackToHighHealth;
        public event CrowdEventHandler PlayerDied;

        public Transform RespawnPoint;
        
        public string HitBox = "JabHitBox";

        public PlayerStatus Status;
        public PlayerState State;

        public Player Grabber;
        public float GrabDamage;

        public bool IsGrounded;
        public bool IsAttacking;
        public bool CollidesWithWall;
        public bool MovementIsStopped;
        public bool AttackIsOnCooldown;
        public bool AbilityIsOnCooldown;
        
        public float HorizontalMovement;
        public float AbilityCooldown = 3f;
        public float AttackCooldown;
        public float Speed;
        public float JumpPower;
        public float Health = 10;
        public float CurrentHealth = 10;
        public float AttackTimestamp;
        public float AttackDuration = 1f;
        public float Damage = 1f;
        public float AbilityDamage = 15f;
        public float RespawnTime = 5;
        public float AttackOffsetX = 1.5f;

        private Rigidbody2D _rigidbody;
        private Transform _attackCheck;
        private Transform _groundCheck;
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;
        private CapsuleCollider2D _collider;
        private SoundManager _soundManager;
        private InputManager _inputManager;
        public IAttackBehaviour _attackBehaviour;
        public IAbility _ability;

        public void Awake()
        {
            var crowdManager = GameObject.Find("Crowd").GetComponent<CrowdManager>();
            PlayerDropedToLowHealth += crowdManager.PlayerDropedToLowHealth;
            PlayerComeBackToHighHealth += crowdManager.PlayerComeBackToHighHealth;
            PlayerDied += crowdManager.PlayerDied;

            _attackBehaviour = GetComponent<IAttackBehaviour>();
            _inputManager = GetComponent<InputManager>();
            _rigidbody = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _collider = GetComponent<CapsuleCollider2D>();
            _soundManager = GetComponent<SoundManager>();
            _ability = GetComponent<IAbility>();

            _attackCheck = transform.Find("AttackCheck");
            _groundCheck = transform.Find("GroundCheck");
        }

        public void Start()
        {
            _soundManager = GetComponent<SoundManager>();
        }

        public void Update()
        {
            switch(Status)
            {
                case PlayerStatus.ALIVE:
                    switch(State)
                    {
                        case PlayerState.IDLE:
                            _inputManager.HandleInputs(this, _animator);
                            _inputManager.HandleMovement(this);
                            break;
                        case PlayerState.JUMPING:
                            _inputManager.HandleInputs(this, _animator);
                            _inputManager.HandleMovement(this);
                            break;
                    }
                    
                    break;
            }

            _animator.SetBool("IsGrounded", IsGrounded);
            _animator.SetFloat("Velocity", _rigidbody.velocity.y);
            _animator.SetFloat("VelocityX", Math.Abs(HorizontalMovement));
            _animator.SetBool("IsIdle", State == PlayerState.IDLE);
            _animator.SetBool("IsAlive", Status == PlayerStatus.ALIVE);
            _animator.SetBool("IsDead", Status == PlayerStatus.DEAD || Status == PlayerStatus.RESPAWNING);
        }

        public void FixedUpdate()
        {
            UpdatePlayerSide();
            
            switch (Status)
            {
                case PlayerStatus.ALIVE:
                    _collider.enabled = true;
                    _rigidbody.bodyType = RigidbodyType2D.Dynamic;
                    if (CurrentHealth <= 20)
                        PlayerDropedToLowHealth(this, null);
                    break;
                case PlayerStatus.DEAD:
                    _rigidbody.bodyType = RigidbodyType2D.Static;
                    _collider.enabled = false;
                    break;
            }
        }

        public int GetDirectionModifier()
        {
            if (_attackCheck.position.x < transform.position.x)
                return -1;
            else
                return 1;
        }

        public void Attack()
        {
            _attackBehaviour.Behave(this);
        }

        public void TriggerAbility()
        {
            _ability.Trigger(this);
        }

        private void Die()
        {
            _soundManager.PlayDeathSound();

            PlayerDied(this, null);
            CurrentHealth = 0;
            Status = PlayerStatus.DEAD;
            CollidesWithWall = false;
            Invoke("Respawn", RespawnTime);
        }

        private void Respawn()
        {
            CurrentHealth = Health;
            Status = PlayerStatus.ALIVE;
            AttackIsOnCooldown = false;
            AbilityIsOnCooldown = false;
            transform.position = RespawnPoint.position;
            PlayerComeBackToHighHealth(this, null);
        }

        public void SuffersDamage(float damage, Vector3 from)
        {
            _animator.SetTrigger("HitReceived");
            KnockbackFrom(from);
            CurrentHealth -= damage;

            if (CurrentHealth <= 0)
            {
                Die();
            }
        }

        public void Land()
        {
            _soundManager.PlayLandSound();
            AnimationManager.Instance().ShowLandImpact(new Vector2(_groundCheck.position.x, _groundCheck.position.y + 2));
        }
        
        public void KnockbackFrom(Vector3 from, float power = 70f)
        {
            State = PlayerState.STUNNED;
            UpdatePlayerSideAfterImpact(from.x);
            _soundManager.PlayHitReceivedSound();
            Invoke("Recover", 0.5f);
            if (from.x < transform.position.x)
                _rigidbody.velocity = new Vector2(power, 20);
            else
                _rigidbody.velocity = new Vector2(-power, 20);
        }

        public void RecoverIn(float time)
        {
            Invoke("Recover", time);
        }

        private void Recover()
        {
            State = PlayerState.IDLE;
        }

        public void UpdatePlayerSideAfterImpact(float impactFromX)
        {
            _animator.SetTrigger("HitReceived");
            if (impactFromX < transform.position.x)
            {
                SwitchToLeft();
            }
            else
            {
                SwitchToRight();
            }
        }

        public void UpdatePlayerSide()
        {
            if (HorizontalMovement != 0)
            {
                if (HorizontalMovement > 0)
                {
                    SwitchToRight();
                }else
                {
                    SwitchToLeft();
                }
            }
        }

        private void SwitchToRight()
        {
            _attackCheck.transform.localPosition = new Vector3(AttackOffsetX, 0, 0);
            if (_spriteRenderer.flipX)
                _spriteRenderer.flipX = false;
        }

        private void SwitchToLeft()
        {
            _attackCheck.transform.localPosition = new Vector3(-AttackOffsetX, 0, 0);
            if (!_spriteRenderer.flipX)
                _spriteRenderer.flipX = true;
        }

        public void StartAttackCooldown()
        {
            AttackIsOnCooldown = true;
            Invoke("ResetAttackCooldown", AttackCooldown);
        }

        public void StartAbilityCooldown(float cooldown = 0)
        {
            AbilityIsOnCooldown = true;
            Invoke("ResetAbilityCooldown", cooldown == 0 ? AbilityCooldown : cooldown);
        }

        private void ResetAttackCooldown()
        {
            AttackIsOnCooldown = false;
        }

        private void ResetAbilityCooldown()
        {
            AbilityIsOnCooldown = false;
        }

        public void OnCollisionEnter2D(Collision2D coll)
        {
            if (CollidesWith(coll, "Wall"))
            {
                if (Math.Abs(_rigidbody.velocity.x) > 0.1)
                    AnimationManager.Instance().ShowSideImpact(coll.contacts[0].point);

                CollidesWithWall = true;
                HorizontalMovement = 0;
                    
            }
            else if (CollidesWith(coll, "Player"))
            {
                HorizontalMovement = 0;
            }
        }

        public void OnCollisionStay2D(Collision2D coll)
        {
            if (CollidesWith(coll, "Wall"))
            {
                HorizontalMovement = 0;
            }
        }

        public void OnCollisionExit2D(Collision2D coll)
        {
            if (CollidesWith(coll, "Wall"))
            {
                CollidesWithWall = false;
            }
        }

        private bool CollidesWith(Collision2D collision, string layer)
        {
            return collision.gameObject.layer == LayerMask.NameToLayer(layer);
        }
    }

    public enum PlayerStatus
    {
        ALIVE,
        DEAD,
        RESPAWNING,
    }

    public enum PlayerState
    {
        IDLE,
        JUMPING,
        STUNNED,
    }
}
