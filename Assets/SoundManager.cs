﻿using UnityEngine;

namespace Assets
{
    public class SoundManager : MonoBehaviour
    {
        public AudioClip DeathClip;
        public AudioClip HitReceivedClip;
        public AudioClip AttackClip;
        public AudioClip FallClip;
        public AudioClip LandClip;
        public AudioClip[] WalkClips;
        public AudioClip JumpClip;
        public AudioSource AudioSource;
        public AudioSource BattleAudioSource;

        public void Awake()
        {
            BattleAudioSource.clip = AttackClip;
        }

        public void PlayJumpSound()
        {
            PlayClip(AudioSource, JumpClip);
        }

        public void PlayAxeHitSound()
        {
            PlayClip(BattleAudioSource, AttackClip);
        }

        public void PlayDeathSound()
        {
            PlayClip(AudioSource, DeathClip);
        }

        public void PlayFallSound()
        {
            PlayClip(AudioSource, FallClip);
        }

        public void PlayLandSound()
        {
            PlayClip(AudioSource, LandClip);
        }

        public void PlayHitReceivedSound()
        {
            PlayClip(AudioSource, HitReceivedClip);
        }

        public void PlayWalkSound()
        {
            var r = new System.Random();
            var i = r.Next(0, 2);

            PlayClip(AudioSource, WalkClips[i]);
        }

        public void PlayClip(AudioSource source, AudioClip clip)
        {
            source.clip = clip;
            source.Play();
        }
    }
}
